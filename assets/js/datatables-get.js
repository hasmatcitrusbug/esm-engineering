$(document).ready(function(){ 

$('#all-project-table').DataTable(
{   "paging":   false,
    "ordering": true,
    "info":     false,
    "bLengthChange": false,
    "searching": false,      
    'columnDefs': [ {
        'targets': [1,2,3,5], 
        'orderable': false, 
    }],
    responsive: true,
});

$('#user-listing-table').DataTable(
{   "paging":   false,
    "ordering": true,
    "info":     false,
    "bLengthChange": false,
    "searching": false,      
    'columnDefs': [ {
        'targets': [1,2,3], 
        'orderable': false, 
    }],
    responsive: true,
});

$('#all-activity-table').DataTable(
    {   "paging":   false,
        "ordering": true,
        "info":     false,
        "bLengthChange": false,
        "searching": false,      
        'columnDefs': [ {
            'targets': [0,1,3],
            'orderable': false, 
        }],
        responsive: true,
});
   
});
