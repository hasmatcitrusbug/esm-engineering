$('#change_show_password-c1').on('click', function(){  

    var passwordField2 = $('#change-password-c1');  
    var passwordFieldType2 = passwordField2.attr('type');
    
    if(passwordField2.val() != '')
    {
        if(passwordFieldType2 == 'password')  
        {  
            passwordField2.attr('type', 'html');  
            $(this).html('<span class="material-icons password-hide">visibility</span>');  
        }  
        else  
        {  
            passwordField2.attr('type', 'password');  
            $(this).html('<span class="material-icons password-view">visibility_off</span>');     
        }
    }
}); 

$('#change_show_password-c2').on('click', function(){  

    var passwordField2 = $('#change-password-c2');  
    var passwordFieldType2 = passwordField2.attr('type');

    if(passwordField2.val() != '')
    {
        if(passwordFieldType2 == 'password')  
        {  
            passwordField2.attr('type', 'html');  
            $(this).html('<span class="material-icons password-hide">visibility</span>');  
        }  
        else  
        {  
            passwordField2.attr('type', 'password');  
            $(this).html('<span class="material-icons password-view">visibility_off</span>');     
        }
    }
}); 

